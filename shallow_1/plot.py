'''
Created on 17 Feb 2016
@author: Charlotte Alexandra Wilson

Last revision: 19 Feb 2016 
see bitbucket for details:
https://CharlotteWilson@bitbucket.org/CharlotteWilson/dnnpd.git
---------------------------------------------------------------

Script for plotting any needed data to graphs for comparisons 
and pattern recognition. Patterns that, I already know, are 
not there. ;_;
---------------------------------------------------------------
'''

#import numpy as np 
import matplotlib.pyplot as pl

# Need to plot the output against the number of layers 
# x axis  = number layers
# y axis = max score

layers = [4,9,14]
maxScore = [0.08, 0.11,0.20,0.12,0.16,0.13,0.21,0.20,0.20,0.18,0.20,0.17,0.13,0.21,0.10,0.08,0.05,0.19 ,0.22,0.20]

pl.plot(learnRate,maxScore)
pl.title("firstLRVariation")
pl.xlabel('learnRate')
pl.ylabel('Score')
pl.savefig("testOutput/" + "LRM0.03E10_10layers_ReversePyramid" + "ts10" + ".png")
# Take in array from scores, need to create array same size

"""
Check length of scoreArray
create new epoch array of same length 
fill epoch array with 0 - length of first array (loop?)

"""

