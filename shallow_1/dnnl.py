'''
Created on 19 Feb 2016
@author: Charlotte Alexandra Wilson

see bitbucket for details:
https://CharlotteWilson@bitbucket.org/CharlotteWilson/dnnpd.git
---------------------------------------------------------------

Script for testing the effects of changing the number of layers
while keeping the other variables, i.e. momentum, learn rates 
and so on constant. 

using the most effective paramaters from previous tests for this, 
to get the best possible results in this. 
---------------------------------------------------------------
'''

import numpy as np
import matplotlib.pyplot as pl
# Just checking how long this takes...
import time
# Neural nets model inputs
#-------------------------
from nolearn.dbn import DBN 
import csv

from tweet import tweet # My own class holding the twitter shit
# Scikit learn imports
#---------------------
# Split into training and testing data
from sklearn.cross_validation import train_test_split
# Generate our pretty table of accuracies
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

import gdbn.activationFunctions as poop

start_time = time.time()

"""
Time for some networks shit
"""
numFeat = 50 * 64 

labs = ["BC", "FL", "PRO1", "PRO2", "RP", "SD"]  # Defect types

data = []  # Features(needs to be matrix)
labels = []  # Target (needs to be vector)
scoreAr = [] # Storing precision scores whoop
hiddenAr = [numFeat, 6] # initial params for dbn
layerAr = []
layerScore = []

layerScore.append([]) # Should be able to change this to instantate at 
layerScore.append([]) # the start?

ep =10
pos = int(len(hiddenAr) - 1) # Second last element of dbn paramater, rep hidden layers
learn_rates = 0.01
mom = 0.03
layer = 10 # Specify max number of hidden layers

for i in range(layer): # Add the number of layers specified
	#hiddenAr.insert(pos, 300) # to the hidden layer array


# Defects csv to get features etc
#--------------------------------
with open("all_defects.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        input = [int(x) for x in row[:numFeat]]  # Cast all inputs to int (2D array - matrix)
        labels += [labs.index(row[-1])]  # categorical labels in numerical form
        data += [input]  # Throw all inputs into data array - now features matrix

# Test output into csv
#---------------------
name = "testOutput/" + "squareELU" + "MaxL%s" %layer + "Ep%s" %ep + "LR%s" %learn_rates + "M%s" %mom
resultFile = csv.writer(open(name + ".csv",'wb')) 

# Generate training data
#-----------------------
X_train, X_test, y_train, y_test = train_test_split(
    data, labels, test_size=0.25) 


for i in range (layer):
	"""
	hiddenAr.insert(pos, 300) # to the hidden layer array
	hiddenAr.insert(pos, 500) # to the hidden layer array
	hiddenAr.insert(pos, 1000) # to the hidden layer array
	hiddenAr.insert(pos, 100) # to the hidden layer array
	"""
	hiddenAr.insert(pos, 300) # to the hidden layer array
	layerScore[0].append(i + 1) # Add layer num to first elem of matrix yas

	# DBN 
	#-----	
	dbn = DBN(
	# [[numNodes input layer], numNodes hidden layer, numNodes output layer ]
	hiddenAr,
	# Learning rate of algorithm
	learn_rates,
	# Decay of learn rate
	learn_rate_decays=1,
	# Iterations of training data (epochs)
	epochs=ep,
	# Verbosity level
	verbose=1,
	momentum=mom,
	) 
        
	dbn.fit(np.asarray(X_train), np.asarray(y_train)) 


	# Evaluation
	#------------
	y_pred = dbn.predict(np.asarray(X_test)) # Predictions 
	confMat = confusion_matrix(y_test, y_pred) # Confusion matrix
	report = classification_report(y_test, y_pred, target_names=labs) # Classification report 
	score = accuracy_score(y_test, y_pred) # Overall precision score

	print score
	print "\n"
	scoreAr.append(score) # Shouldn't need the two arrays for this **** CHANGE PLS****
	layerScore[1].append(score)
	# Test result output
	#------------------
	print report
	print confMat
	print "\n"

	
          
#loop through the array, add elements from it one by one to the csv
for x,y in zip(layerAr,scoreAr):
    resultFile.writerow([x,y]) # layer : score 

# Variables for output
maxScore = max(layerScore[1]) 
# Get the corresponding layer for max score
i = layerScore[1].index(maxScore)
coresLayer = layerScore[0][i]
time = round((time.time() - start_time),2)
# Want to print max score and at what layer number it occured
twitterStr = ("squareELU : Num Layer %s " %coresLayer + " Maxscore: %s" %round(maxScore,2) + " runtime : %s" %time)

#tw = tweet(str(twitterStr)) # Call twitter class to instantiate and then update twitter app
print(twitterStr)

pl.plot(layerScore[0],layerScore[1])
pl.xlabel('layer')
pl.ylabel('score')
pl.savefig(name + ".png")






    
 
    






