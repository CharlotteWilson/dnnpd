'''
Created on 26 Nov 2015
@author: Charlotte Alexandra Wilson
 
Each given file represents one seconds worth of data
Rows: high voltage cycle number (0 - 49) ; power frequency = 50HZ, therefore 1s of data
Columns: different wave position on HV waveform 
There are 64 columns thus, 360 degrees/64 = 5.625 degrees per column

So, if a PD occurred during the second cycle at 10 degrees, it would go in the second row and second column.
Amplitude of the PD pulse is represented by the colour intensity of the pixel(in bmp) or the value of the cell(csv)

Input to neural nets: One of the csv files
There are 64x50 values in the file thus, number of inputs to network = 64x50 = 3200
The number of outputs will be 6 for the 6 different defect types.

'''
import pandas as pd 
import numpy as np
# Neural nets model inputs
#-------------------------
from nolearn.dbn import DBN 
import csv
# Scikit learn imports
#---------------------
# Datasets import
from sklearn import datasets
# Split into training and testing data
from sklearn.cross_validation import train_test_split
# Generate our pretty table of accuracies
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn import metrics

from tweet import tweet # My own class holding the twitter shit
import gdbn.activationFunctions as p


numFeat = 50 * 64 

labs = ["BC", "FL", "PRO1", "PRO2", "RP", "SD"]  # Defect types

data = []  # Features(needs to be matrix)
labels = []  # Target (needs to be vector)

with open("all_defects.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        input = [int(x) for x in row[:numFeat]]  # Cast all inputs to int (2D array - matrix)
        labels += [labs.index(row[-1])]  # categorical labels in numerical form
        data += [input]  # Throw all inputs into data array - now features matrix


# Generate training data
#-----------------------
X_train, X_test, y_train, y_test = train_test_split(
    data, labels, test_size=0.25) 

# DBN 
#-----

dbn = DBN(
# [[numNodes input layer], numNodes hidden layer, numNodes output layer ]
[numFeat, 300, 6],
# Learning rate of algorithm
learn_rates = 0.09,
# Decay of learn rate
learn_rate_decays=1,
# Iterations of training data (epochs)
epochs=10,
# Verbosity level
verbose=1,
use_re_lu=True
) 
    
    
dbn.fit(np.asarray(X_train), np.asarray(y_train)) 


f = open("firstRun.txt", 'a')
    
# Evaluate preds
#-----------------
y_pred = dbn.predict(np.asarray(X_test))
    
confMat = confusion_matrix(y_test, y_pred)
report = classification_report(y_test, y_pred, target_names=labs)
score = metrics.precision_score(y_test, y_pred,pos_label=0) # Check docs

print report
print confMat
print "\n"
print score

f.write(classification_report(y_test, y_pred, target_names=labs));
f.write("\n")
f.write(str(confusion_matrix(y_test, y_pred)));
f.write("\n")
f.write("Precision Score: %s" %score)

#twitterStr = ("ELU: For LR 0.3, Mom 0.9, 10EP, score is : %s" %score)

#tw = tweet(str(twitterStr)) # Call twitter class to instantiate and then update twitter app
#print(twitterStr) 
   
    


    
 
    






