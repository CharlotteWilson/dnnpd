import matplotlib.pyplot as pl

hiddenAr = [100, 6]
layerScore = []

# layer
layerScore.append([])
# score
layerScore.append([])
# learn rate
layerScore.append([])

layerScore[0].append(1)
layerScore[0].append(2)
layerScore[0].append(3)
layerScore[1].append(0.5674)
layerScore[1].append(0.90)
layerScore[1].append(0.0000)
layerScore[2].append(200)
layerScore[2].append(300)
layerScore[2].append(400)


print layerScore
maxScore = max(layerScore[1]) # Get maxScore
i = layerScore[1].index(maxScore) # Get index of max score
print i
print layerScore[0][i] # Get layer to go with this..
print layerScore[2][i] # Get learn rate to go with this...
print maxScore

"""
pl.plot(layerScore[0],layerScore[1],layerScore[2])
pl.xlabel('layer')
pl.ylabel('score')
pl.show()
"""

"""
pos = len(hiddenAr) - 1 # Second last element of dbn paramater, rep hidden layers
layer = 10
layerAr = []

print hiddenAr

for i in range(0,10):
	hiddenAr.insert(pos, 300) # to the hidden layer array
	layerAr.append(i + 1)

print hiddenAr
print layerAr
"""
