'''
Created on 19 Feb 2016
@author: Charlotte Alexandra Wilson

see bitbucket for details:
https://CharlotteWilson@bitbucket.org/CharlotteWilson/dnnpd.git
---------------------------------------------------------------

Script for testing the effects of changing the number of layers
while keeping the other variables, i.e. momentum, learn rates 
and so on constant. 

using the most effective paramaters from previous tests for this, 
to get the best possible results in this. 

Manually changing the number of layers instead of auto since 
there seem to be some errors with npmat while autoadding. ...
which I don't understand.
---------------------------------------------------------------
'''
import numpy as np
import time
from nolearn.dbn import DBN 
import csv
from tweet import tweet 
from sklearn.cross_validation import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

start_time = time.time()


numFeat = 50 * 64 
labs = ["BC", "FL", "PRO1", "PRO2", "RP", "SD"]  # Defect types
data = []  # Features(needs to be matrix)
labels = []  # Target (needs to be vector)
layer = 25
ep =10
learn_rates = 0.03
mom = 0.03


# Defects csv to get features etc
#--------------------------------
with open("all_defects.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        input = [int(x) for x in row[:numFeat]]  # Cast all inputs to int (2D array - matrix)
        labels += [labs.index(row[-1])]  # categorical labels in numerical form
        data += [input]  # Throw all inputs into data array - now features matrix


# Generate training data
#-----------------------
X_train, X_test, y_train, y_test = train_test_split(
    data, labels, test_size=0.25) 



# DBN 
#-----	
dbn = DBN(
# [[numNodes input layer], numNodes hidden layer, numNodes output layer ]
[numFeat, 1504, 100, 300, 500, 1000, 1504, 100, 300, 500, 1000, 1504, 100, 300, 500, 1000, 1504, 100, 300, 500, 1000, 1504, 100, 300, 500, 1000, 6],
# Learning rate of algorithm
learn_rates,
# Decay of learn rate
learn_rate_decays=1,
# Iterations of training data (epochs)
epochs=ep,
# Verbosity level
verbose=1,
momentum=mom,
use_re_lu=True
) 
        
dbn.fit(np.asarray(X_train), np.asarray(y_train)) 


# Evaluation
#------------
y_pred = dbn.predict(np.asarray(X_test)) # Predictions 
confMat = confusion_matrix(y_test, y_pred) # Confusion matrix
report = classification_report(y_test, y_pred, target_names=labs) # Classification report 
score = round(accuracy_score(y_test, y_pred),2) # Overall precision score

print score
print "\n"
print report
print confMat
print "\n"

timeCustom = round((time.time() - start_time),2)

twitterStr = ( "RevPyr_Stretch " + "L %s " %layer + "LR %s " %learn_rates + "M %s "  %mom + "score %s " %score +" runtime : %s" %timeCustom + " test_size = 0.25")
tw = tweet(str(twitterStr)) # Call twitter class to instantiate and then update twitter app
print(twitterStr)	
  





    
 
    






