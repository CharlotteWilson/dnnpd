'''
Created on 26 Nov 2015
@author: Charlotte Alexandra Wilson

Last revision: 19 Feb 2016 
see bitbucket for details:
https://CharlotteWilson@bitbucket.org/CharlotteWilson/dnnpd.git
---------------------------------------------------------------

Initial testing file for paramater variation in order to get the 
best possible accuracy from the network, shallow or otherwise.
---------------------------------------------------------------
'''

import numpy as np
import matplotlib.pyplot as pl
# Just checking how long this takes...
import time
# Neural nets model inputs
#-------------------------
from nolearn.dbn import DBN 
import csv
import random
import string

from tweet import tweet # My own class holding the twitter shit
# Scikit learn imports
#---------------------
# Split into training and testing data
from sklearn.cross_validation import train_test_split
# Generate our pretty table of accuracies
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn import metrics


"""
The random string to stop duplicate tweets.. to a certain extent I suppose
"""
randomstr = ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(16)])
# Check run time of program
start_time = time.time()

"""
Time for some networks shit
"""
numFeat = 50 * 64 

labs = ["BC", "FL", "PRO1", "PRO2", "RP", "SD"]  # Defect types

data = []  # Features(needs to be matrix)
labels = []  # Target (needs to be vector)
scoreAr = [] # Storing precision scores whoop
variableAr = [] # Store testing variable values
momAr = []

maxVariation = 0.10
steps = 10
ep = 10
layers = 1 # Needs to be manually changed
ac = "softMax"
# Defects csv to get features etc
#--------------------------------
with open("all_defects.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        input = [int(x) for x in row[:numFeat]]  # Cast all inputs to int (2D array - matrix)
        labels += [labs.index(row[-1])]  # categorical labels in numerical form
        data += [input]  # Throw all inputs into data array - now features matrix

# Test output into csv
#---------------------
name = "testOutputTanh/" + "smaxFunc" + "L%s" %layers + "Ep%s" %ep + "LRMVar%s" %maxVariation + ".csv" # Generate cvs name based on test variable
resultFile = csv.writer(open(name,'wb')) # File to write results to


# Generate training data
#-----------------------
X_train, X_test, y_train, y_test = train_test_split(
    data, labels, test_size=0.25) 

# DBN 
#-----
for learn_rates in np.linspace(0,maxVariation,num = steps):
    for mom in np.linspace(0,maxVariation,num = steps):
       
        dbn = DBN(
            # [[numNodes input layer], numNodes hidden layer, numNodes output layer ]
            [numFeat, 300, 6],
            # Learning rate of algorithm
            learn_rates,
            # Decay of learn rate
            #learn_rate_decays=1,
            # Iterations of training data (epochs)
            epochs=ep,
            # Verbosity level
            #verbose=1,
            momentum=mom,
            output_act_funct = "Tanh"
            ) 
    
    
        dbn.fit(np.asarray(X_train), np.asarray(y_train)) 

       
    
        # Evaluation
        #------------
        y_pred = dbn.predict(np.asarray(X_test)) # Predictions 
        confMat = confusion_matrix(y_test, y_pred) # Confusion matrix
        report = classification_report(y_test, y_pred, target_names=labs) # Classification report 
        score = metrics.precision_score(y_test, y_pred) # Overall precision score

        print score
        print "\n"
        scoreAr.append(score)
        variableAr.append(learn_rates)
        momAr.append(mom)
        # Test result output
        #------------------
        print report
        print confMat
        print "\n"
        
    
    
#loop through the array, add elements from it one by one to the csv
for x, y, z in zip(scoreAr,variableAr,momAr):
    resultFile.writerow([x,y,z])

# Variables for output 
maxScore = round(max(scoreAr),2)
averageScore = round(np.mean(scoreAr),2)
time = round((time.time() - start_time),2)

# Output to twitter and console
twitterStr = ( "L%s" %layers + "Ep: %s" %ep + " LR-MVar: %s" %maxVariation + " Steps: %s " %steps + "RT: %ss " %time + " Max Score: %s" % maxScore + " 	AvScore: %s" %averageScore)

tw = tweet(str(twitterStr)) # Call twitter class to instantiate and then update twitter app
print(twitterStr) 

"""
Call plot class and put the scores array in there
"""

pl.plot(scoreAr,variableAr,'r')
pl.plot(scoreAr,momAr,'b')
pl.xlabel('Learn_rate - r Momentum - b')
pl.ylabel('Accuracy score')
pl.savefig(name + ".png")

    


    
 
    






