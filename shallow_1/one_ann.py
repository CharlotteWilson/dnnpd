import csv
import png
import numpy as np

from sklearn import preprocessing

from sklearn.cross_validation import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn import metrics

from nolearn.dbn import DBN

from tweet import tweet

ninputs = 50*64
labs = ["BC", "FL", "PRO1", "PRO2", "RP", "SD"]

data = []
labels = []

with open("all_defects.csv") as infile:
    reader = csv.reader(infile)
    for row in reader:
        inputs = [int(x) for x in row[:ninputs]] 
        labels += [labs.index(row[-1])] 
        data += [inputs]

(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.25)

# Build ANN
clf = DBN(
     [ninputs, 300, 300, 6], # length of labels i.e arraylist.size()
    learn_rates=0.03,
    learn_rate_decays=1,
    momentum=0.01,
    epochs=20,
    verbose=1,
    use_re_lu=True
    )

clf.fit(np.asarray(trainX), np.asarray(trainY))

# test
predY = clf.predict(np.asarray(testX))

# Confusion Matrix analysis
print(confusion_matrix(testY, predY))
print(classification_report(testY, predY, target_names=labs))

score = round(metrics.precision_score(testY, predY),4)
print score

twitterStr = ("2 hidden layers, 20E LR.03 M0.01 Score: %s" %score)
tw = tweet(str(twitterStr))