"""
Genetic algorithm example
Trying to create a list of n 
integers that equal x when summed together

if n is 5 and x is 200 then these would all
be appropriate solutions:

[40,40,40,40,40]
[50,50,50,25,25]
[200,0,0,0,0]

Ingredients to the solution! 

Each suggested solution for a genetic algorithm
is referred to as an INDIVIDUAL.

In our current problem, each list of n numbers
is an individual.

The collection of individuals is known as our population

Once we have our population of indoviduals we need to find
a way to judge how effective each solution is; to judge the 
fitness of each inddividual.

This is the fitness function. For our problem, we want the 
fitness to be a function of the distance between the sum of 
an individuals number and the target number x.

It would also be helpful to have a function that will 
determine a populations average fitness.
"""

from random import randint
from operator import add

def individual(length, min, max):
	'create a member of the population.'
	return [randint(min,max) for x in range(length)]

def population(count,length, min, max):
	"""
	Create a number of individuals (i.e population)

	count: number of individuals in the population
	length: num values per individual
	min: min possible values in an individuals list of values
	max: max possible balues in an individuals list of values

	"""
	return[individual(length,min,max) for x in range(count)]

def fitness(individual, target):
	"""
	Determine the fitness of an individual, lower is better.

	individual: the individual to evaluate
	target: the sum of numers that individuals are aiming for.
	"""

	sum = reduce(add, individual, 0)
	return abs(target - sum)

def grade(pop,target):
	'Find average fitness for a population'

	summed = reduce(add, (fitness(x,target) for x in pop), 0)
	return summed / (len(pop) * 1.0)

"""
EVOLUTION
---------

Now we just need a way to evolve our population; to 
advance the population from one generation to the next.

Evolution is the secreat sauce of genetic algorithms oh bby. 
Consider a population of elk which are hunted by a pack of wolves. 
With each generation the weakest are eaten by the wolves, the strongest 
reproduce and have children.

We can implement the evolution mechanism:

1. For each generation we will take a portion of the best performing
   individuals as judged by the fitness function. These high performers will be
   the parents of the next generation.

   We will also randomly select some lesser performing individuals to the parents, 
   because we want to promote genetic diversity.

   One of the dangers of optomizing algorithms is getting stuck at a local maximum 
   and consequently being unable to fine the real maximum. By including some individuals 
   who are not performing well, we decrease our likelihood of getting stuck.

2. Breed together parents to repopulate the population to its desired size (if you take the 
   top 20 individuals in a population of 100, then you need to create 80 new children via 
   breeding). In our case, breeding is pretty basic:

   Take the first n/2 digits from the father and the last n/2 digits from the mother.

"""

father = [1,2,3,4,5,6]
mother = [10,20,30,40,50,60]

child = father[: 3] + mother[3:]
print child

"""
Its ok to have one parent breed multiple times, 
but the same parents should never be both the 
father and mother of a child.

3. Merge together the parents and children to 
   constitue the next generations population

4. Finally we mutate a small random portion of 
   the population. What this means is to have a 
   probability of randomly modifying each individual.

"""

chance_to_mutate = 0.01
for i in population:
	if chance_to_mutate > random();
		place_to_modify = randint(0,len(i))
		i[place_to_modify] = randint(min(i),max(i))