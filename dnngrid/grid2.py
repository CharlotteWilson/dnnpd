'''
Created on 09 March 2016
@author: Charlotte Alexandra Wilson

see bitbucket for details:
https://CharlotteWilson@bitbucket.org/CharlotteWilson/dnnpd.git
---------------------------------------------------------------

Grid search for paramater tuning while also testing the layers. 

using the most effective paramaters from previous tests for this, 
to get the best possible results in this. 
---------------------------------------------------------------
'''

import numpy as np
import matplotlib.pyplot as pl
# Just checking how long this takes...
import time
# Neural nets model inputs
#-------------------------
from nolearn.dbn import DBN 
import csv

from tweet import tweet # My own class holding the twitter shit
# Scikit learn imports
#---------------------
# Split into training and testing data
from sklearn.cross_validation import train_test_split
# Generate our pretty table of accuracies
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

import gdbn.activationFunctions as poop

"""
Time for some networks shit
"""
numFeat = 50 * 64 

labs = ["BC", "FL", "PRO1", "PRO2", "RP", "SD"]  # Defect types

data = []  # Features(needs to be matrix)
labels = []  # Target (needs to be vector)

hiddenAr = [numFeat, 6] # initial params for dbn

tempLayer = []

tempLayer.append([]) # Score
tempLayer.append([]) # Learn rate
tempLayer.append([]) # Momentum

layerScore = []

layerScore.append([]) # Current layer 
layerScore.append([]) # MaxScore
layerScore.append([]) # Learn rate (max)
layerScore.append([]) # Momentum (max)

maxVariation = 0.10
steps = 10
ep = 10
pos = int(len(hiddenAr) - 1) # Second last element of dbn paramater, rep hidden layers
learn_rates = 0.01
mom = 0.03
layer = 10 # Specify max number of hidden layers
start_time = time.time()


# Defects csv to get features etc
#--------------------------------
with open("all_defects.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        input = [int(x) for x in row[:numFeat]]  # Cast all inputs to int (2D array - matrix)
        labels += [labs.index(row[-1])]  # categorical labels in numerical form
        data += [input]  # Throw all inputs into data array - now features matrix

# Test output into csv
#---------------------
name = "GiantGrid_3"+ "L%s" %layer + "Ep%s" %ep + "LRMVar%s" %maxVariation # Generate cvs name based on test variable
resultFile = csv.writer(open(name + ".csv",'wb')) # File to write results to
 
# Generate training data
#-----------------------
X_train, X_test, y_train, y_test = train_test_split(
    data, labels, test_size=0.25) 


for i in range (layer):
	hiddenAr.insert(pos,300)# Add another layer 
	layerScore[0].append(i + 1) # layer num in matrix

	# paramater tuning for each layer
	for learn_rates in np.linspace(0,maxVariation, num = steps):
		for mom in np.linspace(0,maxVariation, num = steps):

			# Add momentum and learn rate to temp layer
			tempLayer[1].append(learn_rates)
			tempLayer[2].append(mom)

			# DBN
			#-----
			dbn = DBN(
			hiddenAr,
			learn_rates,
			learn_rate_decays=1,
			epochs=ep,
			verbose=1,
			momentum=mom,
			)
			
			dbn.fit(np.asarray(X_train), np.asarray(y_train)) 

			# Evaluation
			#------------
			y_pred = dbn.predict(np.asarray(X_test)) # Predictions 
			confMat = confusion_matrix(y_test, y_pred) # Confusion matrix
			report = classification_report(y_test, y_pred, target_names=labs)
			score = accuracy_score(y_test, y_pred) # Overall precision score

			
			# Add score to temp layer array
			tempLayer[0].append(score)

	# Find max from all scores
	maxScore = max(tempLayer[0])
	# Find index for this maxScore
	i = tempLayer[0].index(maxScore)
	# find learn rate for this score
	lrMax = tempLayer[1][i]
	# find momentum for this score
	mMax = tempLayer[2][i]

	
	# add max score to layerScore
	layerScore[1].append(maxScore)
	# add max LR to layerScore
	layerScore[2].append(lrMax)
	# add max M to layerScore
	layerScore[3].append(mMax)

for w,x,y,z in zip(layerScore[0],layerScore[1],layerScore[2],layerScore[3]):
	resultFile.writerow(['Layer','Score','learn_rate','Momentum'])
	resultFile.writerow([w,x,y,z])



f = open("test_3_final.txt", 'a')
f.write("max scores with momentum and learn rates \n")
f.write(str(layerScore))
f.write("\n all scores and momentums etc inner loop \n")
f.write(str(tempLayer))

# Go through each layer, find max score from there and its combo
maxLayerScore = max(layerScore[1])
print maxLayerScore
# index for max score
i = layerScore[1].index(maxLayerScore)
# find learn rate for this score
lrMaxL = layerScore[2][i]
print lrMaxL
# find momentum for this score
mMaxL = layerScore[3][i]
print mMaxL
# find layer for this score
print layerScore[1]
print layerScore[0]
print i
layerMax = layerScore[0][i]
print layerMax
time = round((time.time() - start_time),2)


# Output to twitter and console
twitterStr = ("max score: %s " %maxLayerScore + "layer: %s " %layerMax + "learn_rate: %s " %lrMaxL + "momentum: %s " %mMaxL + "tr: %s" %time)

tw = tweet(str(twitterStr)) # Call twitter class to instantiate and then update twitter app
print(twitterStr) 


