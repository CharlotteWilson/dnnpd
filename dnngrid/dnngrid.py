'''
Created on 09 March 2016
@author: Charlotte Alexandra Wilson

see bitbucket for details:
https://CharlotteWilson@bitbucket.org/CharlotteWilson/dnnpd.git
---------------------------------------------------------------

Grid search for paramater tuning while also testing the layers. 

using the most effective paramaters from previous tests for this, 
to get the best possible results in this. 
---------------------------------------------------------------
'''

import numpy as np
import matplotlib.pyplot as pl
# Just checking how long this takes...
import time
# Neural nets model inputs
#-------------------------
from nolearn.dbn import DBN 
import csv

from tweet import tweet # My own class holding the twitter shit
# Scikit learn imports
#---------------------
# Split into training and testing data
from sklearn.cross_validation import train_test_split
# Generate our pretty table of accuracies
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

import gdbn.activationFunctions as poop

start_time = time.time()

"""
Time for some networks shit
"""
numFeat = 50 * 64 

labs = ["BC", "FL", "PRO1", "PRO2", "RP", "SD"]  # Defect types

data = []  # Features(needs to be matrix)
labels = []  # Target (needs to be vector)
scoreAr = [] # Storing precision scores whoop
hiddenAr = [numFeat, 6] # initial params for dbn

tempLayer = []
tempLayer.append([]) # Score
tempLayer.append([]) # Learn rate
tempLayer.append([]) # Momentum

layerScore = []

layerScore.append([]) # Current layer 
layerScore.append([]) # MaxScore
layerScore.append([]) # Learn rate (max)
layerScore.append([]) # Momentum (max)


maxVariation = 0.10
steps = 2
ep =10
pos = int(len(hiddenAr) - 1) # Second last element of dbn paramater, rep hidden layers
learn_rates = 0.01
mom = 0.03
layer = 2 # Specify max number of hidden layers


# Defects csv to get features etc
#--------------------------------
with open("all_defects.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        input = [int(x) for x in row[:numFeat]]  # Cast all inputs to int (2D array - matrix)
        labels += [labs.index(row[-1])]  # categorical labels in numerical form
        data += [input]  # Throw all inputs into data array - now features matrix
 
# Generate training data
#-----------------------
X_train, X_test, y_train, y_test = train_test_split(
    data, labels, test_size=0.25) 

# Layer Iterations
for i in range (layer):
	hiddenAr.insert(pos, 300) # to the hidden layer array
	layerScore[0].append(i + 1) # Add layer num to first elem of matrix 
	
	# Paramater tuning for each layer 
	for learn_rates in np.linspace(0,maxVariation,num = steps):
    		for mom in np.linspace(0,maxVariation,num = steps):

    			# Add momentum and learn rate to temp layer
    			tempLayer[1].append(learn_rates)
    			tempLayer[2].append(mom)

				# DBN 
				#-----	
				dbn = DBN(
				hiddenAr,
				# Learning rate of algorithm
				learn_rates,
				# Decay of learn rate
				learn_rate_decays=1,
				# Iterations of training data (epochs)
				epochs=ep,
				# Verbosity level
				verbose=1,
				momentum=mom,
				) 
        
		dbn.fit(np.asarray(X_train), np.asarray(y_train)) 


	# Evaluation
	#------------
	y_pred = dbn.predict(np.asarray(X_test)) # Predictions 
	confMat = confusion_matrix(y_test, y_pred) # Confusion matrix
	report = classification_report(y_test, y_pred, target_names=labs) # Classification report 
	score = accuracy_score(y_test, y_pred) # Overall precision score

	print score
	print "\n"
	# Test result output
	#------------------
	print report
	print confMat
	print "\n"

	# Add score to temp layer array
	tempLayer[0].append(score)

	# Now want to iterate through all scores, find max. 
	maxScore = max(tempLayer[0])
	# Find index for max score
	i = tempLayer[0].index(maxScore)
	# Find learn rate for this score
	lrMax = tempLayer[1][i]
	# Find momentum for this score
	mMax = tempLayer[2][i]

	print tempLayer[0][1][2][i] # print this combination

	# add max score to layerScore
	layerScore[1].append(maxScore)
	# add max LR to layerScore
	layerScore[2].append(lrMax)
	# add max M to layerScore
	layerScore[3].append(mMax)

print layerScore
print "\n"
print tempLayer

#time = round((time.time() - start_time),2)
# Want to print max score and at what layer number it occured along with combination LR M
#twitterStr = ("squareELU : Num Layer %s " %coresLayer + " Maxscore: %s" %round(maxScore,2) + " runtime : %s" %time)

#tw = tweet(str(twitterStr)) # Call twitter class to instantiate and then update twitter app
#print(twitterStr)

# Print out full layerScore array
# Print out the max from layerScore array also

# plot each layer with scores (from LayerScore)
#pl.plot(layerScore[0],layerScore[1])
#pl.xlabel('layer')
#pl.ylabel('score')
#pl.savefig(name + ".png")






    
 
    






