"""
Created on 16 March 2016
@author: Charlotte Alexandra Wilson

Simple script for parsing all generated data into one csv.
To be used as input to neural networks for PD fault diagnostics. 
"""

import csv
import glob

"""
Simple parsing of input csv to output csv
** Add iteration for files ***
"""
class parsing:

	"""
	Takes a csv file and inputs every element 
	of it into the row of another csv.

	Param self: 	Passing in self of type String
	Param csv1: 	Input csv of type String	
	Param csv2: 	Output csv of type String
	Param defect: 	Defect label of type String
	"""
	def csvToRow(self, csv1, csv2, defect):
		list1 = []

		with (open(csv1,"rb")) as f:
 			csvreader = csv.reader(f,delimiter = ",")
 			for row in csvreader:
    				for col in row:
    					list1.append(col)

		list1.append(defect)

		inputData = csv.writer(open(csv2, "ab"))
		inputData.writerow(list1)


'Should be able to add below code to parsing class as functions for further use'
instance = parsing()

inData = "neuralNetInput_5000.csv"
filePath = "generated_5000/*/*.csv"

for fileName in glob.glob(filePath):
	#print fileName
	splitName = fileName.split('\\')
	defect = splitName[1]
	print defect
	instance.csvToRow(fileName,inData,defect)
