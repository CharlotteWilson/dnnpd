import csv
import glob
import sys
import random
import re
import png


def generate(g, passes, label):
    for filename in g:
        with open(filename) as infile:
            m = re.search("([a-z0-9]+\.[a-z0-9]+)\.csv$", filename, flags=re.IGNORECASE)
            basename = m.group(1)
            orig = [row.rstrip("\r\n") for row in infile]
        
            for p in range(passes):
                scale = random.randint(1, 10)

                newp = []

                # scale
                for row in orig:
                    newr = [min(int(pd)*scale, 255) for pd in row.split(",")[:-1]]
                    newp += [newr]

                # modify phase
                for i in range(50*64/2): # shift up to half
                    if (random.randint(0, 2)):
                        x = random.randint(0, 49)
                        y = random.randint(1, 63)

                        #print(str(x), str(y))

                        tmp = newp[x][y-1]
                        newp[x][y-1] = newp[x][y]
                        newp[x][y] = tmp


                newfile = basename+"-"+str(p)
                with open(outdir+label+"/"+newfile+".csv", "w") as outc:
                    c = csv.writer(outc)
                    c.writerows(newp)

                with open(outdir+label+"/"+newfile+".png", "wb") as outp:
                    w = png.Writer(64, 50, greyscale=True)
                    w.write(outp, newp)



#patterns = int(sys.argv[1]) # 1st argument is how many patterns
patterns = 5000

outdir = "generated_5000/"

g = glob.glob("subset/BC/*.csv")
passes = patterns/len(g)
generate(g, passes, "BC")

g = glob.glob("subset/FL/*.csv")
passes = patterns/len(g)
generate(g, passes, "FL")

g = glob.glob("subset/PRO/*.csv")
passes = patterns/len(g)
generate(g, passes, "PRO")

g = glob.glob("subset/PRO2/*.csv")
passes = patterns/len(g)
generate(g, passes, "PRO2")

g = glob.glob("subset/RP/*.csv")
passes = patterns/len(g)
generate(g, passes, "RP")

g = glob.glob("subset/SD/*.csv")
passes = patterns/len(g)
generate(g, passes, "SD")

