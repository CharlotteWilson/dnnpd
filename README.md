# README #

EE475 project CES 2016
##Deep Neural Networks for Detecting Partial Discharge Defects in Power Network Equipment##

* Deep Belief networks set up to differentiate between 6 different PD defects.
* Working Version - needs cleaned up.


### Getting Started! ###

* For numpy, scipy sklearn etc download Anaconda 2.7: https://www.continuum.io/downloads
* Install Anaconda 2.7 following setup then install nolearn 0.5 using conda or pip
* Install tweepy https://github.com/tweepy/tweepy
* May need to install PNG, csv modules also. Should be easy to install anything else using:
"pip install module" or "conda install module"

* Most networks are run using Shallow_clean.py (single network)
* ShallowBC.py for grids
* Most file names are self explanatory
 
* More Documentation, code clean up and branch merging to come. 

### Questions? ###

Any questions etc, send me a message through Bitbucket.